#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_rcc.h"
#include "stm32f4xx_hal_spi.h"


#define MASTER_SPI_SCK_PIN 				GPIO_PIN_3
#define MASTER_SPI_SCK_PORT 			GPIOB

#define MASTER_SPI_MISO_PIN 			GPIO_PIN_4
#define MASTER_SPI_MISO_PORT 			GPIOB

#define MASTER_SPI_MOSI_PIN 			GPIO_PIN_5
#define MASTER_SPI_MOSI_PORT 			GPIOB

#define MASTER_SPI_INT1_PIN 			GPIO_PIN_1        
#define MASTER_SPI_INT1_PORT 			GPIOB

#define MASTER_SPI_AF							GPIO_AF6_SPI3

#define MASTER_SPI_EXT_IRQ				EXTI1_IRQn

#define MASTER_SPI_INSTANCE `			SPI3


/* initialize spi master and spi gpios and data ready interrupt*/
void initializeSPI_IO (void); 
void HAL_SPI_MspInit(SPI_HandleTypeDef* hspi);
void EXTI1_IRQHandler();
//void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);
void Receive();
