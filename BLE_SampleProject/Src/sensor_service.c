/**
  ******************************************************************************
  * @file    sensor_service.c
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    04-July-2014
  * @brief   Add a sample service using a vendor specific profile.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2014 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
#include "sensor_service.h"
#include "Thread_SPI.h"

/** @addtogroup X-CUBE-BLE1_Applications
 *  @{
 */

/** @addtogroup SensorDemo
 *  @{
 */
 
/** @defgroup SENSOR_SERVICE
 * @{
 */

/** @defgroup SENSOR_SERVICE_Private_Variables
 * @{
 */
/* Private variables ---------------------------------------------------------*/
volatile int connected = FALSE;
volatile uint8_t set_connectable = 1;
volatile uint16_t connection_handle = 0;
volatile uint8_t notification_enabled = FALSE;
uint16_t sampleServHandle, TXCharHandle, RXCharHandle;
uint16_t accServHandle, pitchCharHandle, rollCharHandle;
uint16_t tempServHandle, tempCharHandle;
uint16_t dtapServHandle, dtapCharHandle;
uint16_t ledServHandle, ledPatCharHandle, ledDutyCharHandle,ledSpeedCharHandle;
uint16_t fortuneServHandle, fortuneCharHandle;
uint16_t composerServHandle, composerCharHandle;
uint16_t shakeServHandle, shakeCharHandle;
uint16_t pitchDescHandle, rollDescHandle, tempDescHandle, dutyDescHandle, dtapDescHandle;
uint16_t fortuneDescHandle, composerDescHandle;
uint8_t update_spi[2];

#if NEW_SERVICES
  uint16_t timeServHandle, secondsCharHandle, minuteCharHandle;
  uint16_t ledServHandle, ledButtonCharHandle;
  uint8_t ledState = 0;
  int previousMinuteValue = -1;
  extern uint8_t bnrg_expansion_board;
#endif
/**
 * @}
 */

/** @defgroup SENSOR_SERVICE_Private_Macros
 * @{
 */
/* Private macros ------------------------------------------------------------*/
#define COPY_UUID_128(uuid_struct, uuid_15, uuid_14, uuid_13, uuid_12, uuid_11, uuid_10, uuid_9, uuid_8, uuid_7, uuid_6, uuid_5, uuid_4, uuid_3, uuid_2, uuid_1, uuid_0) \
do {\
    uuid_struct[0] = uuid_0; uuid_struct[1] = uuid_1; uuid_struct[2] = uuid_2; uuid_struct[3] = uuid_3; \
        uuid_struct[4] = uuid_4; uuid_struct[5] = uuid_5; uuid_struct[6] = uuid_6; uuid_struct[7] = uuid_7; \
            uuid_struct[8] = uuid_8; uuid_struct[9] = uuid_9; uuid_struct[10] = uuid_10; uuid_struct[11] = uuid_11; \
                uuid_struct[12] = uuid_12; uuid_struct[13] = uuid_13; uuid_struct[14] = uuid_14; uuid_struct[15] = uuid_15; \
}while(0)

/** Service Definitions **/

#define COPY_TEMP_SERVICE_UUID(uuid_struct)        COPY_UUID_128(uuid_struct,0xf0,0x00,0xaa,0x20, 0x04,0x51, 0x40,0x00, 0xb0,0x00, 0x00,0x00,0x00,0x00,0x00,0x00)
#define COPY_TEMP_CHAR_UUID(uuid_struct)           COPY_UUID_128(uuid_struct,0xf0,0x00,0xaa,0x21, 0x04,0x51, 0x40,0x00, 0xb0,0x00, 0x00,0x00,0x00,0x00,0x00,0x00)
#define COPY_TEMP_CONFIG_CHAR_UUID(uuid_struct)    COPY_UUID_128(uuid_struct,0xf0,0x00,0xaa,0x21, 0x04,0x51, 0x40,0x00, 0xb0,0x00, 0x00,0x00,0x00,0x00,0x00,0x00) 

#define COPY_ACC_SERVICE_UUID(uuid_struct)         COPY_UUID_128(uuid_struct,0xf0,0x00,0xaa,0x40, 0x04,0x51, 0x40,0x00, 0xb0,0x00, 0x00,0x00,0x00,0x00,0x00,0x00)
#define COPY_PITCH_UUID(uuid_struct)               COPY_UUID_128(uuid_struct,0xf0,0x00,0xaa,0x41, 0x04,0x51, 0x40,0x00, 0xb0,0x00, 0x00,0x00,0x00,0x00,0x00,0x00)
#define COPY_PITCH_CONFIG_UUID(uuid_struct)        COPY_UUID_128(uuid_struct,0xf0,0x00,0xaa,0x41, 0x04,0x51, 0x40,0x00, 0xb0,0x00, 0x00,0x00,0x00,0x00,0x00,0x00)
#define COPY_ROLL_UUID(uuid_struct)                COPY_UUID_128(uuid_struct,0xf0,0x00,0xaa,0x42, 0x04,0x51, 0x40,0x00, 0xb0,0x00, 0x00,0x00,0x00,0x00,0x00,0x00)
#define COPY_ROLL_CONFIG_UUID(uuid_struct)         COPY_UUID_128(uuid_struct,0xf0,0x00,0xaa,0x41, 0x04,0x51, 0x40,0x00, 0xb0,0x00, 0x00,0x00,0x00,0x00,0x00,0x00)

#define COPY_DOUBLE_TAP_SERVICE_UUID(uuid_struct)  COPY_UUID_128(uuid_struct,0xf0,0x00,0xaa,0x60, 0x04,0x51, 0x40,0x00, 0xb0,0x00, 0x00,0x00,0x00,0x00,0x00,0x00)
#define COPY_DOUBLE_TAP_CHAR_UUID(uuid_struct)     COPY_UUID_128(uuid_struct,0xf0,0x00,0xaa,0x61, 0x04,0x51, 0x40,0x00, 0xb0,0x00, 0x00,0x00,0x00,0x00,0x00,0x00)

#define COPY_LED_SERVICE_UUID(uuid_struct)         COPY_UUID_128(uuid_struct,0xf0,0x00,0xaa,0x80, 0x04,0x51, 0x40,0x00, 0xb0,0x00, 0x00,0x00,0x00,0x00,0x00,0x00)
#define COPY_LED_PATERN_CHAR_UUID(uuid_struct)     COPY_UUID_128(uuid_struct,0xf0,0x00,0xaa,0x81, 0x04,0x51, 0x40,0x00, 0xb0,0x00, 0x00,0x00,0x00,0x00,0x00,0x00)
#define COPY_LED_DUTY_CHAR_UUID(uuid_struct)       COPY_UUID_128(uuid_struct,0xf0,0x00,0xaa,0x82, 0x04,0x51, 0x40,0x00, 0xb0,0x00, 0x00,0x00,0x00,0x00,0x00,0x00)
#define COPY_LED_SPEED_CHAR_UUID(uuid_struct)      COPY_UUID_128(uuid_struct,0xf0,0x00,0xaa,0x83, 0x04,0x51, 0x40,0x00, 0xb0,0x00, 0x00,0x00,0x00,0x00,0x00,0x00)

#define COPY_FORTUNE_SERVICE_UUID(uuid_struct) 		 COPY_UUID_128(uuid_struct,0xf0,0x00,0xaa,0xa0, 0x04,0x51, 0x40,0x00, 0xb0,0x00, 0x00,0x00,0x00,0x00,0x00,0x00)
#define COPY_FORTUNE_CHAR_UUID(uuid_struct)    		 COPY_UUID_128(uuid_struct,0xf0,0x00,0xaa,0xa1, 0x04,0x51, 0x40,0x00, 0xb0,0x00, 0x00,0x00,0x00,0x00,0x00,0x00)

#define COPY_COMPOSER_SERVICE_UUID(uuid_struct) 	 COPY_UUID_128(uuid_struct,0xf0,0x00,0xaa,0xc0, 0x04,0x51, 0x40,0x00, 0xb0,0x00, 0x00,0x00,0x00,0x00,0x00,0x00)
#define COPY_COMPOSER_CHAR_UUID(uuid_struct)    	 COPY_UUID_128(uuid_struct,0xf0,0x00,0xaa,0xc1, 0x04,0x51, 0x40,0x00, 0xb0,0x00, 0x00,0x00,0x00,0x00,0x00,0x00)

#define COPY_SHAKE_SERVICE_UUID(uuid_struct) 	 		 COPY_UUID_128(uuid_struct,0xf0,0x00,0xaa,0xe0, 0x04,0x51, 0x40,0x00, 0xb0,0x00, 0x00,0x00,0x00,0x00,0x00,0x00)
#define COPY_SHAKE_CHAR_UUID(uuid_struct)    	 		 COPY_UUID_128(uuid_struct,0xf0,0x00,0xaa,0xe1, 0x04,0x51, 0x40,0x00, 0xb0,0x00, 0x00,0x00,0x00,0x00,0x00,0x00)

#define COPY_CONFIG_DESCRIPTOR_UUID(uuid_struct)   COPY_UUID_128(uuid_struct,0x00,0x00,0x29,0x02, 0x00,0x00, 0x10,0x10, 0x80,0x00, 0x00,0x80,0x5f,0x9b,0x34,0xfb)
#define COPY_SCONFIG_DESCRIPTOR_UUID(uuid_struct)  COPY_UUID_128(uuid_struct,0x00,0x00,0x29,0x03, 0x00,0x00, 0x10,0x10, 0x80,0x00, 0x00,0x80,0x5f,0x9b,0x34,0xfb)



/* Store Value into a buffer in Little Endian Format */
#define STORE_LE_16(buf, val)    ( ((buf)[0] =  (uint8_t) (val)    ) , \
                                   ((buf)[1] =  (uint8_t) (val>>8) ) )
/**
 * @}
 */

/** @defgroup SENSOR_SERVICE_Exported_Functions 
 * @{
 */ 
/**
 * @brief  Add an accelerometer service with pitch and roll characteristics.
 *
 * @param  None
 * @retval tBleStatus Status
 */
tBleStatus Add_Acc_Service(void)
{
  tBleStatus ret;

  uint8_t uuid[16];
  
  COPY_ACC_SERVICE_UUID(uuid);
  ret = aci_gatt_add_serv(UUID_TYPE_128,  uuid, PRIMARY_SERVICE, 15,
                          &accServHandle);
  if (ret != BLE_STATUS_SUCCESS) goto fail;

  COPY_PITCH_UUID(uuid);
  ret =  aci_gatt_add_char(accServHandle, UUID_TYPE_128, uuid, 2,
                           CHAR_PROP_NOTIFY|CHAR_PROP_READ, ATTR_PERMISSION_NONE, 0,
                           16, 0, &pitchCharHandle);
  if (ret != BLE_STATUS_SUCCESS) goto fail;

	COPY_CONFIG_DESCRIPTOR_UUID(uuid);
	ret = aci_gatt_add_char_desc(accServHandle, pitchCharHandle, UUID_TYPE_128, uuid, 4,
																4,0, ATTR_PERMISSION_NONE,ATTR_ACCESS_READ_WRITE, 
																GATT_NOTIFY_ATTRIBUTE_WRITE, 16, 0, &pitchDescHandle); 
	if (ret != BLE_STATUS_SUCCESS) goto fail;
  
	COPY_ROLL_UUID(uuid);  
  ret =  aci_gatt_add_char(accServHandle, UUID_TYPE_128, uuid, 2,
                           CHAR_PROP_NOTIFY|CHAR_PROP_READ, ATTR_PERMISSION_NONE, 0,
                           16, 0, &rollCharHandle);											 
  if (ret != BLE_STATUS_SUCCESS) goto fail;
	PRINTF("PASS 3.\n");
	
	COPY_CONFIG_DESCRIPTOR_UUID(uuid);
	ret = aci_gatt_add_char_desc(accServHandle, rollCharHandle, UUID_TYPE_128, uuid, 4,
																4,0, ATTR_PERMISSION_NONE,ATTR_ACCESS_READ_WRITE, 
																GATT_NOTIFY_ATTRIBUTE_WRITE, 16, 0, &rollDescHandle); 
	if (ret != BLE_STATUS_SUCCESS) goto fail;

  PRINTF("Service ACC added. Handle 0x%04X, Free fall Charac handle: 0x%04X, Acc Charac handle: 0x%04X\n",accServHandle, pitchCharHandle, rollCharHandle);	
  return BLE_STATUS_SUCCESS; 
  
fail:
  PRINTF("Error while adding ACC service.\n");
  return BLE_STATUS_ERROR ;
        
}


/**
 * @brief      Add the temperature service
 *
 * @return     Where the temperature service was added successfully
 */
tBleStatus Add_Temp_Service(void)
{
	tBleStatus ret;

  uint8_t uuid[16];
  
  COPY_TEMP_SERVICE_UUID(uuid);
	ret = aci_gatt_add_serv(UUID_TYPE_128,  uuid, PRIMARY_SERVICE, 7,
                          &tempServHandle);
  if (ret != BLE_STATUS_SUCCESS) goto fail;
	
	COPY_TEMP_CHAR_UUID(uuid);
	ret =  aci_gatt_add_char(tempServHandle, UUID_TYPE_128, uuid, 2,
                           CHAR_PROP_NOTIFY|CHAR_PROP_READ|CHAR_PROP_INDICATE, ATTR_PERMISSION_NONE, 0,
                           16, 0, &tempCharHandle);
  if (ret != BLE_STATUS_SUCCESS) goto fail;
	
	COPY_CONFIG_DESCRIPTOR_UUID(uuid);
	ret = aci_gatt_add_char_desc(tempServHandle, tempCharHandle, UUID_TYPE_128, uuid, 4, 4, 0, 
																ATTR_PERMISSION_NONE, ATTR_ACCESS_READ_WRITE, GATT_NOTIFY_ATTRIBUTE_WRITE, 16, 0, &tempDescHandle);
	if (ret != BLE_STATUS_SUCCESS) goto fail;
	return BLE_STATUS_SUCCESS; 
fail:
  PRINTF("Error while adding Temp service.\n");
  return BLE_STATUS_ERROR ;
}


/**
 * @brief      Adds the LED service. Adds pattern duty and speed characteristics
 *
 * @return     IF the service was set up sucessfully
 */
tBleStatus Add_LED_Service(void)
{
	tBleStatus ret;

  uint8_t uuid[16];
	
	COPY_LED_SERVICE_UUID(uuid);
	ret = aci_gatt_add_serv(UUID_TYPE_128,  uuid, PRIMARY_SERVICE, 7,
                          &ledServHandle);
  if (ret != BLE_STATUS_SUCCESS) goto fail;
	
	COPY_LED_DUTY_CHAR_UUID(uuid);
	ret =  aci_gatt_add_char(ledServHandle, UUID_TYPE_128, uuid, 2,
                           CHAR_PROP_WRITE_WITHOUT_RESP|CHAR_PROP_READ, ATTR_PERMISSION_NONE, GATT_NOTIFY_ATTRIBUTE_WRITE,
                           16, 0, &ledDutyCharHandle);
  if (ret != BLE_STATUS_SUCCESS) goto fail;
	
	COPY_LED_PATERN_CHAR_UUID(uuid);
	ret =  aci_gatt_add_char(ledServHandle, UUID_TYPE_128, uuid, 2,
                           CHAR_PROP_WRITE_WITHOUT_RESP|CHAR_PROP_READ , ATTR_PERMISSION_NONE, GATT_NOTIFY_ATTRIBUTE_WRITE,
                           16, 0, &ledPatCharHandle);
  if (ret != BLE_STATUS_SUCCESS) goto fail;
	
	COPY_LED_SPEED_CHAR_UUID(uuid);
	ret =  aci_gatt_add_char(ledServHandle, UUID_TYPE_128, uuid, 2,
                           CHAR_PROP_WRITE_WITHOUT_RESP|CHAR_PROP_READ, ATTR_PERMISSION_NONE, GATT_NOTIFY_ATTRIBUTE_WRITE,
                           16, 0, &ledSpeedCharHandle);
  if (ret != BLE_STATUS_SUCCESS) goto fail;
	
	return BLE_STATUS_SUCCESS; 
fail:
  PRINTF("Error while adding Temp service.\n");
  return BLE_STATUS_ERROR ;
}

/**
 * @brief      Adds the double tap service
 *
 * @return     If the double tap service was added succesfully
 */
tBleStatus Add_Double_Tap_Service(void)
{
	tBleStatus ret;
	
	uint8_t uuid[16];
	
	COPY_DOUBLE_TAP_SERVICE_UUID(uuid);
	ret = aci_gatt_add_serv(UUID_TYPE_128, uuid, PRIMARY_SERVICE, 10, &dtapServHandle);
	if (ret != BLE_STATUS_SUCCESS) goto fail;
	
	COPY_DOUBLE_TAP_CHAR_UUID(uuid);
	ret =  aci_gatt_add_char(dtapServHandle, UUID_TYPE_128, uuid, 2,
                           CHAR_PROP_NOTIFY|CHAR_PROP_READ, ATTR_PERMISSION_NONE, 0,
                           16, 0, &dtapCharHandle);
	if (ret != BLE_STATUS_SUCCESS) goto fail;
	
	COPY_CONFIG_DESCRIPTOR_UUID(uuid);
	ret = aci_gatt_add_char_desc(dtapServHandle, dtapCharHandle, UUID_TYPE_128, uuid, 4, 4, 0, 
																ATTR_PERMISSION_NONE, ATTR_ACCESS_READ_WRITE, GATT_NOTIFY_ATTRIBUTE_WRITE, 16, 0, &dtapDescHandle);
	if (ret != BLE_STATUS_SUCCESS) goto fail;
	
	return BLE_STATUS_SUCCESS; 
	
fail:
  PRINTF("Error while adding Duble Tap service.\n");
  return BLE_STATUS_ERROR ;
}

/**
 * @brief      Adds the fortune service
 *
 * @return     If the service was added successfully
 */
tBleStatus Add_Fortune_Service(void)
{
	tBleStatus ret;
	
	uint8_t uuid[16];
	
	COPY_FORTUNE_SERVICE_UUID(uuid);
	ret = aci_gatt_add_serv(UUID_TYPE_128, uuid, PRIMARY_SERVICE, 7, &fortuneServHandle);
	if (ret != BLE_STATUS_SUCCESS) goto fail;
	
	COPY_FORTUNE_CHAR_UUID(uuid);
	ret =  aci_gatt_add_char(fortuneServHandle, UUID_TYPE_128, uuid, 2,
                           CHAR_PROP_NOTIFY|CHAR_PROP_READ, ATTR_PERMISSION_NONE, 0,
                           16, 0, &fortuneCharHandle);
	if (ret != BLE_STATUS_SUCCESS) goto fail;
	
	COPY_CONFIG_DESCRIPTOR_UUID(uuid);
	ret = aci_gatt_add_char_desc(fortuneServHandle, fortuneCharHandle, UUID_TYPE_128, uuid, 4, 4, 0, 
																ATTR_PERMISSION_NONE, ATTR_ACCESS_READ_WRITE, GATT_NOTIFY_ATTRIBUTE_WRITE, 16, 0, &fortuneDescHandle);
	if (ret != BLE_STATUS_SUCCESS) goto fail;
	
	return BLE_STATUS_SUCCESS; 

	
fail:
  PRINTF("Error while adding Temp service.\n");
  return BLE_STATUS_ERROR ;
}

/**
 * @brief      Added the composer services
 *
 * @return     IF teh service was added succesfully
 */
tBleStatus Add_Composer_Service(void)
{
	tBleStatus ret;
	
	uint8_t uuid[16];
	
	COPY_COMPOSER_SERVICE_UUID(uuid);
	ret = aci_gatt_add_serv(UUID_TYPE_128, uuid, PRIMARY_SERVICE, 7, &composerServHandle);
	if (ret != BLE_STATUS_SUCCESS) goto fail;
	
	COPY_COMPOSER_CHAR_UUID(uuid);
	ret =  aci_gatt_add_char(composerServHandle, UUID_TYPE_128, uuid, 2,
                           CHAR_PROP_NOTIFY|CHAR_PROP_READ, ATTR_PERMISSION_NONE, 0,
                           16, 0, &composerCharHandle);
	if (ret != BLE_STATUS_SUCCESS) goto fail;
	
	COPY_CONFIG_DESCRIPTOR_UUID(uuid);
	ret = aci_gatt_add_char_desc(composerServHandle, composerCharHandle, UUID_TYPE_128, uuid, 4, 4, 0, 
																ATTR_PERMISSION_NONE, ATTR_ACCESS_READ_WRITE, GATT_NOTIFY_ATTRIBUTE_WRITE, 16, 0, &composerDescHandle);
	if (ret != BLE_STATUS_SUCCESS) goto fail;
	
	return BLE_STATUS_SUCCESS; 

	
fail:
  PRINTF("Error while adding Temp service.\n");
  return BLE_STATUS_ERROR ;
}


/**
 * @brief      ADds the shake service
 *
 * @return     If the shake service was added successfully
 */
tBleStatus Add_Shake_Service(void)
{
	tBleStatus ret;
	
	uint8_t uuid[16];
	
	COPY_SHAKE_SERVICE_UUID(uuid);
	ret = aci_gatt_add_serv(UUID_TYPE_128, uuid, PRIMARY_SERVICE, 7, &shakeServHandle);
	if (ret != BLE_STATUS_SUCCESS) goto fail;
	
	COPY_SHAKE_CHAR_UUID(uuid);
	ret =  aci_gatt_add_char(shakeServHandle, UUID_TYPE_128, uuid, 2,
                           CHAR_PROP_WRITE_WITHOUT_RESP|CHAR_PROP_READ, ATTR_PERMISSION_NONE, GATT_NOTIFY_ATTRIBUTE_WRITE,
                           16, 0, &shakeCharHandle);
	if (ret != BLE_STATUS_SUCCESS) goto fail;
	
	return BLE_STATUS_SUCCESS; 

	
fail:
  PRINTF("Error while adding Shake service.\n");
  return BLE_STATUS_ERROR ;
}

/**
 * @brief      Updates the value of the pitch anfle
 *
 * @param[in]  value  The value that we want to update with
 *
 * @return     If the update was successful
 */
tBleStatus Pitch_Update(float value)
{
	tBleStatus ret;
	uint8_t buff[2];
	uint16_t val = (uint16_t) (value*100);
	
	STORE_LE_16(buff, val);
	
	ret = aci_gatt_update_char_value(accServHandle, pitchCharHandle, 0, 2, buff);
	
	if (ret != BLE_STATUS_SUCCESS){
    PRINTF("Error while updating Pitch characteristic.\n") ;
    return BLE_STATUS_ERROR ;
  }
  return BLE_STATUS_SUCCESS;
}

/**
 * @brief      Updates the value of the roll anfle
 *
 * @param[in]  value  The value that we want to update with
 *
 * @return     If the update was successful
 */
tBleStatus Roll_Update(float value)
{
	tBleStatus ret;
	uint8_t buff[2];
	uint16_t val = (uint16_t) (value*100);
	
	STORE_LE_16(buff, val);
	
	ret = aci_gatt_update_char_value(accServHandle, rollCharHandle, 0, 2, buff);
	
	if (ret != BLE_STATUS_SUCCESS){
    PRINTF("Error while updating Roll characteristic.\n") ;
    return BLE_STATUS_ERROR ;
  }
  return BLE_STATUS_SUCCESS;
}

/**
 * @brief  Update temperature characteristic value.
 * @param  Temperature in tenths of degree 
 * @retval Status
 */
tBleStatus Temp_Update(float value)
{
	tBleStatus ret;
	uint8_t buff[2];
	uint16_t val = (uint16_t) (value*100);
	
	STORE_LE_16(buff, val);
	
	ret = aci_gatt_update_char_value(tempServHandle, tempCharHandle, 0, 2, buff);
	
	if (ret != BLE_STATUS_SUCCESS){
    PRINTF("Error while updating Temperature characteristic.\n") ;
    return BLE_STATUS_ERROR ;
  }
  return BLE_STATUS_SUCCESS;
}

/**
 * @brief      Updates the fortune
 *
 * @param[in]  value  A value that we want to send.
 *
 * @return     If teh update was succesful
 */
tBleStatus Fortune_Update(uint16_t value)
{
	tBleStatus ret;
	uint8_t buff[2];
	
	STORE_LE_16(buff, value);
	
	ret = aci_gatt_update_char_value(fortuneServHandle, fortuneCharHandle, 0, 2, buff);
	
	if (ret != BLE_STATUS_SUCCESS){
    PRINTF("Error while updating Temperature characteristic.\n") ;
    return BLE_STATUS_ERROR ;
  }
  return BLE_STATUS_SUCCESS;
}

/**
 * @brief      Updatese the composer service
 *
 * @param[in]  value  The value of the key that was pressed from the keypad
 *
 * @return     If teh update was successful
 */
tBleStatus Composer_Update(uint16_t value)
{
	tBleStatus ret;
	uint8_t buff[2];
	
	STORE_LE_16(buff, value);
	
	ret = aci_gatt_update_char_value(composerServHandle, composerCharHandle, 0, 2, buff);
	
	if (ret != BLE_STATUS_SUCCESS){
    PRINTF("Error while updating Temperature characteristic.\n") ;
    return BLE_STATUS_ERROR ;
  }
  return BLE_STATUS_SUCCESS;
}

/**
 * @brief      Notifiies the adnroid device thta a double tap has occured
 *
 * @param[in]  value  A value to end to the android device
 *
 * @return     IF the value was sent succesfully
 */
tBleStatus DoubleTap_Update(uint16_t value)
{
	tBleStatus ret;
	uint8_t buff[2];
	
	STORE_LE_16(buff, value);
	
	ret = aci_gatt_update_char_value(dtapServHandle, dtapCharHandle, 0, 2, buff);
	
	if (ret != BLE_STATUS_SUCCESS){
    PRINTF("Error while updating Temperature characteristic.\n") ;
    return BLE_STATUS_ERROR ;
  }
  return BLE_STATUS_SUCCESS;
	
	
}

/**
 * @brief      The callback that is called when a characteristics gets modified from android
 *  
 * 
 *
 * @param[in]  handle       The handle of the characterisic
 * @param[in]  data_length  The size of teh data
 * @param      att_data     A pointer to the data
 */
void Attribute_Modified_CB(uint16_t handle, uint8_t data_length, uint8_t *att_data)
{

	PRINTF("Attribute modified %d %d\n",handle, *att_data);
	if (handle == ledDutyCharHandle + 1 )
	{
		PRINTF("Got PWM value %d\n", *att_data);
		update_spi[0]=0;
		update_spi[1]= *att_data& 0xff;
		SPI_Transmit(update_spi);
	} else if(handle == ledPatCharHandle + 1)
	{
		PRINTF("Got led Pattern value %d\n", *att_data);
		update_spi[0]=1;
		update_spi[1]= *att_data& 0xff;
		SPI_Transmit(update_spi);
	} else if(handle == ledSpeedCharHandle +1)
	{
		PRINTF("Got led Speed value %d\n", *att_data);
		update_spi[0]=2;
		update_spi[1]= *att_data& 0xff;
		SPI_Transmit(update_spi);
	} else if(handle == shakeCharHandle + 1)
	{
		PRINTF("Got shake! value %d\n", *att_data);
		update_spi[0]=1;
		update_spi[1]=14; 
		SPI_Transmit(update_spi);
	}
}


/**
 * @brief  Puts the device in connectable mode.
 *         If you want to specify a UUID list in the advertising data, those data can
 *         be specified as a parameter in aci_gap_set_discoverable().
 *         For manufacture data, aci_gap_update_adv_data must be called.
 * @param  None 
 * @retval None
 */
/* Ex.:
 *
 *  tBleStatus ret;    
 *  const char local_name[] = {AD_TYPE_COMPLETE_LOCAL_NAME,'B','l','u','e','N','R','G'};    
 *  const uint8_t serviceUUIDList[] = {AD_TYPE_16_BIT_SERV_UUID,0x34,0x12};    
 *  const uint8_t manuf_data[] = {4, AD_TYPE_MANUFACTURER_SPECIFIC_DATA, 0x05, 0x02, 0x01};
 *  
 *  ret = aci_gap_set_discoverable(ADV_IND, 0, 0, PUBLIC_ADDR, NO_WHITE_LIST_USE,
 *                                 8, local_name, 3, serviceUUIDList, 0, 0);    
 *  ret = aci_gap_update_adv_data(5, manuf_data);
 *
 */
void setConnectable(void)
{  
  tBleStatus ret;
  
  const char local_name[] = {AD_TYPE_COMPLETE_LOCAL_NAME,'G','0','3','-','N','R','G'};
  
  /* disable scan response */
  hci_le_set_scan_resp_data(0,NULL);
  PRINTF("General Discoverable Mode.\n");
  
  ret = aci_gap_set_discoverable(ADV_IND, 0, 0, PUBLIC_ADDR, NO_WHITE_LIST_USE,
                                 sizeof(local_name), local_name, 0, NULL, 0, 0);
  if (ret != BLE_STATUS_SUCCESS) {
    PRINTF("Error while setting discoverable mode (%d)\n", ret);    
  }  
}

/**
 * @brief  This function is called when there is a LE Connection Complete event.
 * @param  uint8_t Address of peer device
 * @param  uint16_t Connection handle
 * @retval None
 */
void GAP_ConnectionComplete_CB(uint8_t addr[6], uint16_t handle)
{  
  connected = TRUE;
  connection_handle = handle;
  
  PRINTF("Connected to device:");
  for(int i = 5; i > 0; i--){
    PRINTF("%02X-", addr[i]);
  }
  PRINTF("%02X\n", addr[0]);
}

/**
 * @brief  This function is called when the peer device gets disconnected.
 * @param  None 
 * @retval None
 */
void GAP_DisconnectionComplete_CB(void)
{
  connected = FALSE;
  PRINTF("Disconnected\n");
  /* Make the device connectable again. */
  set_connectable = TRUE;
  notification_enabled = FALSE;
}

/**
 * @brief  Read request callback.
 * @param  uint16_t Handle of the attribute
 * @retval None
 */
void Read_Request_CB(uint16_t handle)
{  
  
  //EXIT:
  if(connection_handle != 0)
    aci_gatt_allow_read(connection_handle);
}

/**
 * @brief  Callback processing the ACI events.
 * @note   Inside this function each event must be identified and correctly
 *         parsed.
 * @param  void* Pointer to the ACI packet
 * @retval None
 */
void HCI_Event_CB(void *pckt)
{
  hci_uart_pckt *hci_pckt = pckt;
  /* obtain event packet */
  hci_event_pckt *event_pckt = (hci_event_pckt*)hci_pckt->data;
  
  if(hci_pckt->type != HCI_EVENT_PKT)
    return;
  
  switch(event_pckt->evt){
    
  case EVT_DISCONN_COMPLETE:
    {
      GAP_DisconnectionComplete_CB();
    }
    break;
    
  case EVT_LE_META_EVENT:
    {
      evt_le_meta_event *evt = (void *)event_pckt->data;
      
      switch(evt->subevent){
      case EVT_LE_CONN_COMPLETE:
        {
          evt_le_connection_complete *cc = (void *)evt->data;
          GAP_ConnectionComplete_CB(cc->peer_bdaddr, cc->handle);
        }
        break;
      }
    }
    break;
    
  case EVT_VENDOR:
    {
      evt_blue_aci *blue_evt = (void*)event_pckt->data;
      switch(blue_evt->ecode){

#if NEW_SERVICES
      case EVT_BLUE_GATT_ATTRIBUTE_MODIFIED:         
        {
          /* this callback is invoked when a GATT attribute is modified
          extract callback data and pass to suitable handler function */
          if (bnrg_expansion_board == IDB05A1) {
            evt_gatt_attr_modified_IDB05A1 *evt = (evt_gatt_attr_modified_IDB05A1*)blue_evt->data;
            Attribute_Modified_CB(evt->attr_handle, evt->data_length, evt->att_data); 
          }
          else {
            evt_gatt_attr_modified_IDB04A1 *evt = (evt_gatt_attr_modified_IDB04A1*)blue_evt->data;
            Attribute_Modified_CB(evt->attr_handle, evt->data_length, evt->att_data); 
          }                       
        }
        break; 
#endif
				case EVT_BLUE_GATT_ATTRIBUTE_MODIFIED:         
        {
          /* this callback is invoked when a GATT attribute is modified
          extract callback data and pass to suitable handler function */
					evt_gatt_attr_modified_IDB04A1 *evt = (evt_gatt_attr_modified_IDB04A1*)blue_evt->data;
					Attribute_Modified_CB(evt->attr_handle, evt->data_length, evt->att_data);                      
        }
        break; 

      case EVT_BLUE_GATT_READ_PERMIT_REQ:
        {
          evt_gatt_read_permit_req *pr = (void*)blue_evt->data;                    
          Read_Request_CB(pr->attr_handle);                    
        }
        break;
      }
    }
    break;
  }    
}
/**
 * @}
 */
 
/**
 * @}
 */

/**
 * @}
 */

 /**
 * @}
 */
 
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
