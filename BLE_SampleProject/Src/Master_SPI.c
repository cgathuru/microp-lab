#include "MASTER_SPI.h"
#include <string.h>
#include <stdio.h>
#include "debug.h"

extern float temperature;
extern float roll;
extern float pitch;
uint32_t temp;
uint32_t r;
uint32_t p;
osThreadId = tid_Master_SPI;
osThreadDef(Master_SPI,osPriorityAboveNormal,1,0);
extern uint8_t interrupt;
uint8_t receive_data[12][1];
SPI_HandleTypeDef spi_handler;
void setSPIRxneFlag(SPI_HandleTypeDef *hspi);
void clearSPIRxneFlag(SPI_HandleTypeDef *hspi);
uint8_t readSPI(SPI_HandleTypeDef *hspi);

/*----------------------------------------------------------------------------
 *      Create the thread within RTOS context
 *---------------------------------------------------------------------------*/
int start_Master_SPI

void Receive(){
 	HAL_SPI_Receive(&spi_handler,receive_data,12,20);
	temp= ((uint32_t)receive_data[0]<<24 )|((uint32_t)receive_data[1]<<16 )| ((uint32_t)receive_data[2]<<8 )|((uint32_t)receive_data[3]);
	r= ((uint32_t)receive_data[4]<<24 )|((uint32_t)receive_data[5]<<16 )| ((uint32_t)receive_data[6]<<8 )|((uint32_t)receive_data[7]);
	p=((uint32_t)receive_data[8]<<24 )|((uint32_t)receive_data[9]<<16 )| ((uint32_t)receive_data[10]<<8 )|((uint32_t)receive_data[11]);
	temperature= * (float *)&temp;
	roll=* (float *)&r;
	pitch=* (float *)&p;
	interrupt=0;
}

void initializeSPI_IO (void){
		
		
		__SPI3_CLK_ENABLE();
		
		HAL_SPI_DeInit(&spi_handler);
		spi_handler.Instance											=	SPI3;
		spi_handler.Init.BaudRatePrescaler				=	SPI_BAUDRATEPRESCALER_64;
		spi_handler.Init.Direction 								= SPI_DIRECTION_2LINES;
		spi_handler.Init.CLKPhase 								= SPI_PHASE_1EDGE;
		spi_handler.Init.CLKPolarity 							= SPI_POLARITY_LOW;
		spi_handler.Init.CRCCalculation						= SPI_CRCCALCULATION_DISABLED;
		spi_handler.Init.CRCPolynomial 						= 7;
		spi_handler.Init.DataSize 								= SPI_DATASIZE_8BIT;
		spi_handler.Init.FirstBit 								= SPI_FIRSTBIT_MSB;
		spi_handler.Init.NSS 											= SPI_NSS_SOFT;
		spi_handler.Init.TIMode 									= SPI_TIMODE_DISABLED;
		spi_handler.Init.Mode 										= SPI_MODE_SLAVE; //initialize as spi master
	//	spi_handler.Init.Direction								= SPI_DIRECTION_2LINES;
		
		__GPIOA_CLK_ENABLE();
		__GPIOB_CLK_ENABLE();
		
	
	
	if (HAL_SPI_Init(&spi_handler) != HAL_OK) {printf ("ERROR: Error in initialising SPI3 \n");};
		
		__HAL_SPI_ENABLE(&spi_handler);
	
		HAL_NVIC_EnableIRQ(MASTER_SPI_EXT_IRQ);
		HAL_NVIC_SetPriority(MASTER_SPI_EXT_IRQ,0,0);
	}

	void setSPIRxneFlag(SPI_HandleTypeDef *hspi){
		hspi->Instance->SR = hspi->Instance->SR & SPI_SR_RXNE;
	}
	
	void clearSPIRxneFlag(SPI_HandleTypeDef *hspi){
		hspi->Instance->SR = hspi->Instance->SR |(~SPI_SR_RXNE);
	}

 uint8_t readSPI(SPI_HandleTypeDef *hspi){
	return hspi->Instance->DR;
 }
	void EXTI1_IRQHandler(){
		if (__HAL_GPIO_EXTI_GET_IT(EXTI1_IRQn) != RESET)      //In case other interrupts are also running
    {
				 __HAL_GPIO_EXTI_CLEAR_IT(EXTI1_IRQn);
					/*own code*/
					interrupt=1;
					Receive();
    }
	}
	
