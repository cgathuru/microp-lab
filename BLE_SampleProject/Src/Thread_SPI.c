#include "Thread_SPI.h"
#include <string.h>
#include <stdio.h>
#include  "math.h"
#include "sensor_service.h"

extern float temperature;
extern float roll;
extern float pitch;
uint32_t temp;
uint32_t r;
uint32_t p;
extern uint8_t interrupt;
uint8_t receive_data[13];
uint8_t receive_hw[2];
SPI_HandleTypeDef spi_handler;
SPI_HandleTypeDef master_spi_handler;
void setSPIRxneFlag(SPI_HandleTypeDef *hspi);
void clearSPIRxneFlag(SPI_HandleTypeDef *hspi);
uint8_t readSPI(SPI_HandleTypeDef *hspi);
int tickStart;
uint8_t dummy_byte[2];
int i;
uint8_t flag;
/*----------------------------------------------------------------------------
*      Thread  'Thread_SPI':  Transmit and receive spi signal
 *---------------------------------------------------------------------------*/
void SPI_Receive(){
		
		//start reading all 12 bytes

		HAL_SPI_Receive(&spi_handler,receive_hw,1,0);
		if(receive_hw[0]<13){
			receive_data[receive_hw[0]]=receive_hw[1];
		}
		temp= ((uint32_t)receive_data[0]<<24 )|((uint32_t)receive_data[1]<<16 )| ((uint32_t)receive_data[2]<<8 )|((uint32_t)receive_data[3]);
		r= ((uint32_t)receive_data[4]<<24 )|((uint32_t)receive_data[5]<<16 )| ((uint32_t)receive_data[6]<<8 )|((uint32_t)receive_data[7]);
		p=((uint32_t)receive_data[8]<<24 )|((uint32_t)receive_data[9]<<16 )| ((uint32_t)receive_data[10]<<8 )|((uint32_t)receive_data[11]);

		
		//end of receive,update values and transmit dummy byte
		if(receive_hw[0]==12){
			interrupt++;
			if(* (float *)&temp>20 && * (float *)&temp<60 ) {
				temperature= * (float *)&temp;
			}
			if(* (float *)&r<180){
				roll=* (float *)&r;
			}
			if(* (float *)&p<180){
				pitch=* (float *)&p;
			}
			//read from receive_data[12]
			if(receive_data[12]==1 ){
				//DoubleTap_Update(receive_data[12]);
				receive_data[12]=0;
			 flag =1;
			}
				//set transmit flag
			//interrupt++;
		}
	}

	void SPI_Transmit(uint8_t *send_data){
		//generate interrupt
		HAL_GPIO_WritePin(MASTER_SPI_INT1_PORT,MASTER_SPI_INT1_PIN,GPIO_PIN_RESET);
		HAL_SPI_Transmit(&master_spi_handler,send_data,1,0);
		HAL_Delay(1);
		HAL_GPIO_WritePin(MASTER_SPI_INT1_PORT,MASTER_SPI_INT1_PIN,GPIO_PIN_SET);
	}
	

/*----------------------------------------------------------------------------
 *      Initialize the GPIO associated with the SPI
 *---------------------------------------------------------------------------*/
void initializeSPI_IO (void){
		
		
		__SPI3_CLK_ENABLE();
		
		HAL_SPI_DeInit(&spi_handler);
		spi_handler.Instance											=	SPI3;
		spi_handler.Init.BaudRatePrescaler				=	SPI_BAUDRATEPRESCALER_64;
		spi_handler.Init.Direction 								= SPI_DIRECTION_2LINES;
		spi_handler.Init.CLKPhase 								= SPI_PHASE_1EDGE;
		spi_handler.Init.CLKPolarity 							= SPI_POLARITY_LOW;
		spi_handler.Init.CRCCalculation						= SPI_CRCCALCULATION_DISABLED;
		spi_handler.Init.CRCPolynomial 						= 7;
		spi_handler.Init.DataSize 								= SPI_DATASIZE_16BIT;
		spi_handler.Init.FirstBit 								= SPI_FIRSTBIT_MSB;
		spi_handler.Init.NSS 											= SPI_NSS_SOFT;
		spi_handler.Init.TIMode 									= SPI_TIMODE_DISABLED;
		spi_handler.Init.Mode 										= SPI_MODE_SLAVE; //initialize as spi slave
		
		__GPIOA_CLK_ENABLE();
		__GPIOB_CLK_ENABLE();
		
	
	
	if (HAL_SPI_Init(&spi_handler) != HAL_OK) {printf ("ERROR: Error in initialising SPI3 \n");};
		
		__HAL_SPI_ENABLE(&spi_handler);
	
		HAL_NVIC_EnableIRQ(SLAVE_SPI_EXT_IRQ);
		HAL_NVIC_SetPriority(SLAVE_SPI_EXT_IRQ,-1,-1);
	
		configureMasterSPI_IO(); //initialize second spi
		
	}
	void configureMasterSPI_IO(){
		__SPI2_CLK_ENABLE();
		HAL_SPI_DeInit(&master_spi_handler);
		master_spi_handler.Instance											=SPI2;
		master_spi_handler.Init.BaudRatePrescaler				=	SPI_BAUDRATEPRESCALER_64;
		master_spi_handler.Init.Direction 							= SPI_DIRECTION_2LINES;
		master_spi_handler.Init.CLKPhase 								= SPI_PHASE_1EDGE;
		master_spi_handler.Init.CLKPolarity 						= SPI_POLARITY_LOW;
		master_spi_handler.Init.CRCCalculation					= SPI_CRCCALCULATION_DISABLED;
		master_spi_handler.Init.CRCPolynomial 					= 7;
		master_spi_handler.Init.DataSize 								= SPI_DATASIZE_16BIT;
		master_spi_handler.Init.FirstBit 								= SPI_FIRSTBIT_MSB;
		master_spi_handler.Init.NSS 										= SPI_NSS_SOFT;
		master_spi_handler.Init.TIMode 									= SPI_TIMODE_DISABLED;
		master_spi_handler.Init.Mode 										= SPI_MODE_MASTER; //initialize as spi master

		__GPIOC_CLK_ENABLE();
		__GPIOB_CLK_ENABLE();
		
		if (HAL_SPI_Init(&master_spi_handler) != HAL_OK) {printf ("ERROR: Error in initialising SPI3 \n");};
		
		__HAL_SPI_ENABLE(&master_spi_handler);
	}
	
	void setSPIRxneFlag(SPI_HandleTypeDef *hspi){
		hspi->Instance->SR = hspi->Instance->SR & SPI_SR_RXNE;
	}
	
	void clearSPIRxneFlag(SPI_HandleTypeDef *hspi){
		hspi->Instance->SR = hspi->Instance->SR |(~SPI_SR_RXNE);
	}

 uint8_t readSPI(SPI_HandleTypeDef *hspi){
	return hspi->Instance->DR;
 }
 
 /*custom HAL delay function*/
	void HAL_Delay(__IO uint32_t Delay)
{
  uint32_t tickstart = 0;
  tickstart = HAL_GetTick();
  while((HAL_GetTick() - tickstart) < Delay)
  {
  }
}

	void EXTI1_IRQHandler(){
		if (__HAL_GPIO_EXTI_GET_IT(SLAVE_SPI_INT1_PIN) != RESET)      //In case other interrupts are also running
    {

			__HAL_GPIO_EXTI_CLEAR_IT(SLAVE_SPI_INT1_PIN);
					/*own code*/
			SPI_Receive();				
    }
	}
	
